#
# Copyright (C) 2018-2022 StatiXOS
#
# SPDX-License-Identifier: Apache-2.0
#

# Include librsjni explicitly to workaround GMS issue
PRODUCT_PACKAGES += \
    librsjni

# BtHelper
PRODUCT_PACKAGES += \
    BtHelper

# Camera
PRODUCT_PACKAGES += \
    Aperture

# Charger images
PRODUCT_PACKAGES += \
    charger_res_images \
    charger_res_images_vendor_pixel

-include vendor/aosp/config/overlay.mk
