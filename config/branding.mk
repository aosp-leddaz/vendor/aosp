#
# Copyright (C) 2018-2022 StatiXOS
#
# SPDX-License-Identifier: Apache-2.0
#

## Signing
ifneq (eng,$(TARGET_BUILD_VARIANT))
    # Define security directory
    PROD_CERTS := vendor/aosp/build/target/product/security

    # Release keys
    ifneq (,$(wildcard $(PROD_CERTS)/releasekey.pk8))
        PRODUCT_DEFAULT_DEV_CERTIFICATE := $(PROD_CERTS)/releasekey
        # OEM unlock
        PRODUCT_DEFAULT_PROPERTY_OVERRIDES += ro.oem_unlock_supported=1
    endif

    # OTA keys
    ifneq (,$(wildcard $(PROD_CERTS)/otakey.x509.pem))
        PRODUCT_OTA_PUBLIC_KEYS := $(PROD_CERTS)/otakey.x509.pem
    endif
endif
